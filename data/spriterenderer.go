package data

type Rect struct {
	X, Y, W, H int32
}

//
type SpriteRenderer struct {
	OffsetX, OffsetY int32
	ScaleX, ScaleY   int32
	graphics         map[string]Rect
}

func NewSpriteRenderer(OffsetX, OffsetY, ScaleX, ScaleY int32) SpriteRenderer {
	return SpriteRenderer{OffsetX, OffsetY, ScaleX, ScaleY, nil}
}

func (self *SpriteRenderer) Destroy() {
}

func (self *SpriteRenderer) NewRect(x, y int32) Rect {
	return Rect{X: x * self.OffsetX, Y: y * self.OffsetY, W: self.OffsetX, H: self.OffsetX}
}

func (self *SpriteRenderer) InitializeGraphics(data map[string][2]int32) {
	self.graphics = make(map[string]Rect)
	for s, e := range data {
		self.graphics[s] = self.NewRect(e[0], e[1])
	}
}

func (self *SpriteRenderer) PixelToSpriteDim(x, y int32) (int32, int32) {
	return x / (self.ScaleX * self.OffsetX), y / (self.ScaleY * self.OffsetY)
}
