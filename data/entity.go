package data

const (
	CommandKindNone   int32 = iota
	CommandKindMove         = iota
	CommandKindSetPos       = iota
)

type Command interface{}

type CommandMove struct {
	Kind             int32
	ID               int64
	OffsetX, OffsetY int32
}

type CommandSetPos struct {
	Kind int32
	ID   int64
	X, Y int32
}

const (
	AI_NONE       int32 = iota
	AI_PASSIVE          = iota
	AI_AGGRESSIVE       = iota
)

type Entity struct {
	name    string
	graphic string
	level   string
	id      int64
	x, y    int32
	NPC     bool
	AI      int32
}

func NewEntity(graphic string, id int64) *Entity {
	return &Entity{"", graphic, "", id, 0, 0, false, AI_NONE}
}

func NewEntityAll(name, graphic, level string, id int64, x, y int32) *Entity {
	return &Entity{name, graphic, level, id, x, y, false, AI_NONE}
}

func (self *Entity) GetGraphic() string {
	return self.graphic
}

func (self *Entity) SetName(name string) {
	self.name = name
}

func (self Entity) GetName() string {
	return self.name
}

func (self *Entity) SetLevel(level string) {
	self.level = level
}

func (self Entity) GetLevel() string {
	return self.level
}

func (self *Entity) SetID(id int64) {
	self.id = id
}

func (self Entity) GetID() int64 {
	return self.id
}

func (self *Entity) SetX(x int32) {
	self.x = x
}

func (self *Entity) SetY(y int32) {
	self.y = y
}

func (self Entity) GetX() int32 {
	return self.x
}

func (self Entity) GetY() int32 {
	return self.y
}

func (self Entity) GetPos() (int32, int32) {
	return self.x, self.y
}

func (self *Entity) SetPos(x, y int32) {
	self.x = x
	self.y = y
}

func (self *Entity) Move(x, y int32) {
	self.x += x
	self.y += y
}

func (self *Entity) SendCommand(command Command) bool {
	result := false
	switch c := command.(type) {
	case CommandMove:
		self.Move(c.OffsetX, c.OffsetY)
		result = true
	case CommandSetPos:
		self.SetPos(c.X, c.Y)
		result = true
	}
	return result
}

func (self *Entity) Update() {

}
