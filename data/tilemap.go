package data

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
)

type Tile struct {
	Solid bool
}

type TileSet struct {
	Tiles map[int32]Tile
}

type TileSetJson struct {
	Tiles map[string]Tile
}

type TileSetRef struct {
	ID   int32
	File string
}

type Layer struct {
	Order   int32
	Name    string
	Tiles   [][]Tile
	Data    [][]int32
	TileSet int32
}

type Tilemap struct {
	Tiles       map[int32]*TileSet
	TileSets    []TileSetRef
	MapWidth    int32
	MapHeight   int32
	PlayerSpawn [2]int32
	Layers      []Layer
}

func NewTilemapFromJson(filename string) *Tilemap {
	fileData, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	tilemap := new(Tilemap)
	json.Unmarshal(fileData, &tilemap)

	tilemap.Tiles = make(map[int32]*TileSet, len(tilemap.TileSets))
	for i := 0; i < len(tilemap.TileSets); i++ {
		t := &tilemap.TileSets[i]
		file, err := ioutil.ReadFile(t.File)
		if err != nil {
			panic(err)
		}
		var tilesetJson TileSetJson
		json.Unmarshal(file, &tilesetJson)
		tiles := make(map[int32]Tile, len(tilesetJson.Tiles))
		tileset := new(TileSet)
		for i, tile := range tilesetJson.Tiles {
			num, _ := strconv.Atoi(i)
			tiles[int32(num)] = tile
		}
		tiles[-1] = Tile{Solid: false}
		tileset.Tiles = tiles
		tilemap.Tiles[t.ID] = tileset
	}

	for i := 0; i < len(tilemap.Layers); i++ {
		layer := &tilemap.Layers[i]
		tileset := tilemap.Tiles[layer.TileSet]
		layer.Tiles = make([][]Tile, tilemap.MapWidth)
		var x int32
		for x = 0; x < tilemap.MapWidth; x++ {
			layer.Tiles[x] = make([]Tile, tilemap.MapHeight)
			var y int32
			for y = 0; y < tilemap.MapHeight; y++ {
				id := layer.Data[x][y]
				layer.Tiles[x][y] = tileset.Tiles[id]
			}
		}
	}
	return tilemap
}

func (self *Tilemap) GetTileByPos(x, y int32) []Tile {
	fmt.Printf("GetTileByPos: x %d y %d\n", x, y)
	if x < 0 || y < 0 || x >= self.MapWidth || y >= self.MapHeight {
		var tiles []Tile
		return tiles
	}
	tiles := make([]Tile, len(self.Layers))
	for i, layer := range self.Layers {
		tiles[i] = layer.Tiles[x][y]
	}
	return tiles
}
