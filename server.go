package main

import (
	"database/sql"
	"fmt"
	"game/data"
	"net"
	"os"
	"time"

	sqlite3 "github.com/mattn/go-sqlite3"
)

const (
	MaxClientAmount   = 50
	SentSize          = 50
	DefaultUpdateTime = int64(time.Millisecond * 100)
	DefaultTimeOut    = int64(time.Second * 5)
	MaxUsernameLength = 16
	MinUsernamelength = 4
)

type ServerClient struct {
	id           int64
	startPing    int64
	startMsgID   string
	lastUpdate   int64
	pingTime     int64 // average amount of time to receive a reply
	sentMessages map[string]NetworkMessage
	salt         string
	address      *net.UDPAddr
	username     string
	is_connected bool
}

func MakeServerClient(id int64, salt string, address *net.UDPAddr) *ServerClient {
	client := new(ServerClient)
	client.id = id
	client.pingTime = DefaultUpdateTime
	client.sentMessages = make(map[string]NetworkMessage, SentSize)
	client.salt = salt
	client.address = address
	return client
}

func (self *ServerClient) RemoveSent(id string) {
	delete(self.sentMessages, id)
}

type ProcInfo struct {
	msg  NetworkMessage
	addr *net.UDPAddr
}

type Server struct {
	id            int32
	salt          string
	connection    *net.UDPConn
	quit          chan int32
	read          chan NetworkMessage
	proc          chan ProcInfo
	clientIDs     int64
	clients       map[int64]*ServerClient
	clientsBySalt map[string]*ServerClient
	database      *sql.DB
}

func NewServer(addr string) Server {
	network, _ := net.ResolveUDPAddr("udp", addr)
	conn, err := net.ListenUDP("udp", network)
	if err != nil {
		panic(err)
	}

	_, err = os.Stat("./gameData.db")
	exists := !os.IsNotExist(err)

	hasSqlite := false
	drivers := sql.Drivers()
	for _, driver := range drivers {
		if driver == "sqlite3" {
			hasSqlite = true
		}
	}
	if !hasSqlite {
		sql.Register("sqlite3", &sqlite3.SQLiteDriver{})
	}

	database, err := sql.Open("sqlite3", "./gameData.db")
	if err != nil {
		panic(err)
	}

	if !exists {
		InitializeDatabase(database)
	}

	clients := make(map[int64]*ServerClient, MaxClientAmount)
	read := make(chan NetworkMessage)
	quit := make(chan int32)
	proc := make(chan ProcInfo)

	clientsBySalt := make(map[string]*ServerClient, MaxClientAmount)
	salt := GenerateSalt()

	return Server{-1, salt, conn, quit, read, proc, 0, clients, clientsBySalt, database}
}

func ServerRead(server *Server, read <-chan NetworkMessage, quit chan int32) {
	for {
		select {
		case <-quit:
			return
		default:
		}
		server.connection.SetReadDeadline(time.Now().Add(time.Millisecond * 100))
		msg, n, addr, err := Read(server.connection)
		if err != nil && !err.(net.Error).Timeout() {
			panic(err)
		}
		if n > 0 && msg != nil {
			server.proc <- ProcInfo{msg, addr}
		}
	}
}

func (self *Server) StartProcessing() {
	for {
		select {
		case <-self.quit:
			return
		case info := <-self.proc:
			data := info.msg.GetMsgData()
			switch m := info.msg.(type) {
			case *PingNetworkMessage:
				self.HandlePing(m, info.addr)
			}
			if client, ok := self.clients[data.PlayerID]; ok && client.is_connected {
				switch m := info.msg.(type) {
				case *PlayerLeftMessage:
					self.HandlePlayerLeft(m, client)
				case *PlayerMovedMessage:
					self.HandlePlayerMove(m)
				case *AckMessage:
					self.HandleAck(m, client)
				case *ChatNetworkMessage:
					self.HandleChatMessage(m)
				}
			} else {
				switch m := info.msg.(type) {
				case *ConnectNetworkMessage:
					self.HandleConnect(m, info.addr)
				case *ConnectionChallengeReplyMessage:
					self.HandleChallengeReply(m, info.addr)
				case *RegisterMessage:
					self.HandleRegister(m, info.addr)
				}
			}
		}
	}
}

func (self *Server) ProcessLostPackets() {
	for {
		select {
		case <-self.quit:
			return
		}
		for _, client := range self.clients {
			timePassed := time.Now().UnixNano() - client.lastUpdate
			if timePassed >= client.pingTime && len(client.sentMessages) > 0 {
				self.ResendAllPackets(client)
				client.lastUpdate = time.Now().UnixNano()
			}
			if (client.startPing - time.Now().UnixNano()) >= DefaultTimeOut {
				self.HandlePlayerTimeout(client)
			}
		}
		time.Sleep(time.Nanosecond * 500)
	}
}

func GetMsgID() string {
	return string(time.Now().UnixNano())
}

func (self *Server) GetSalt() string {
	return self.salt
}

func (self *Server) NewMsgData(msgType int32, client *ServerClient) MessageData {
	// Don't XORSalt in reply because that gives the client the answer to our check
	var salt string
	var id int64
	if client != nil {
		salt = XORSalts(self.salt, client.salt)
	} else {
		salt = self.salt
	}
	if client != nil {
		id = client.id
	}
	return MessageData{msgType, id, GetMsgID(), salt}
}

func (self *Server) Start() {
	go ServerRead(self, self.read, self.quit)
	go self.StartProcessing()
	go self.ProcessLostPackets()
}

func (self *Server) Quit() {
	self.quit <- 0
}

func (self *Server) AddClient(client *ServerClient) {
	self.clients[client.id] = client
	self.clientsBySalt[XORSalts(self.salt, client.salt)] = client
}

func (self *Server) RemoveClient(id int64) {
	client := self.clients[id]
	delete(self.clientsBySalt, client.salt)
	delete(self.clients, id)
}

func (self *Server) SendToAll(msg NetworkMessage) {
	for _, client := range self.clients {
		if client.is_connected {
			self.SendToClient(msg, client)
		}
	}
}

func (self *Server) SendToAllExcept(msg NetworkMessage, except *ServerClient) {
	for _, client := range self.clients {
		if client != except && client.is_connected {
			self.SendToClient(msg, client)
		}
	}
}

func (self *Server) SendToClient(msg NetworkMessage, client *ServerClient) {
	client.sentMessages[msg.GetMsgData().ID] = msg
	if client.startPing == 0 { // it'll be 0 if the server isn't already keeping track
		client.startPing = time.Now().UnixNano()
		client.startMsgID = msg.GetMsgData().ID
	}
	Send(msg, self.connection, client.address)
}

func (self *Server) ResendAllPackets(client *ServerClient) {
	for _, msg := range client.sentMessages {
		Send(msg, self.connection, client.address)
	}
}

func (self *Server) HandleRegister(msg *RegisterMessage, addr *net.UDPAddr) {
	reply := RegisterReplyMessage{self.NewMsgData(RegisterReply, nil), RegisterSuccessful}
	entity := GetUserFromName(self.database, msg.Username)
	if entity != nil {
		reply.Status = RegisterUsernameTaken
	} else {
		if len(msg.Username) > MaxUsernameLength {
			reply.Status = RegisterUsernameTooLong
		} else if len(msg.Username) < MinUsernamelength {
			reply.Status = RegisterUsernameTooShort
		}
	}
	if reply.Status == RegisterSuccessful {
		// TODO(): eventually retrieve player spawn location from GameManager
		spawn := gameManager.GetPlayerSpawn()
		CreateUser(self.database, msg.Username, msg.Password, msg.Graphic, "level0", spawn[0], spawn[1])
	}

	Send(reply, self.connection, addr)
}

func (self *Server) HandleConnect(msg *ConnectNetworkMessage, address *net.UDPAddr) {
	entity := GetUserFromName(self.database, msg.Username)
	if entity == nil {
		Send(ConnectionDeclinedMessage{self.NewMsgData(ConnectionDeclined, nil), DeclinedNotRegistered}, self.connection, address)
		return
	}
	ok := TestUserPassword(self.database, msg.Username, msg.Password)
	if !ok {
		Send(ConnectionDeclinedMessage{self.NewMsgData(ConnectionDeclined, nil), DeclinedWrongPassword}, self.connection, address)
		return
	}
	reply := ConnectionChallengeMessage{self.NewMsgData(ConnectionChallenge, nil), self.salt}
	Send(reply, self.connection, address)
}

func (self *Server) HandleChallengeReply(msg *ConnectionChallengeReplyMessage, address *net.UDPAddr) {
	// if _, ok := self.pending[msg.PlayerID]; ok {
	// 	// if msg.MsgData.Salt != XORSalts(self.salt, c.salt) {
	// 	// 	Send(ConnectionDeclinedMessage{self.NewMsgData(ConnectionDeclined, c), DeclinedWrongSalt}, self.connection, address)
	// 	// 	return
	// 	// }
	// } else {
	// 	return
	// }

	// first try to get user
	entity := GetUserFromName(self.database, msg.Username)
	if entity == nil {
		Send(ConnectionDeclinedMessage{self.NewMsgData(ConnectionDeclined, nil), DeclinedNotRegistered}, self.connection, address)
	}
	client := MakeServerClient(entity.GetID(), msg.GetMsgData().Salt, address)
	client.username = entity.GetName()

	client.is_connected = true
	// send a message to all clients that a client has connected
	joinedMsg := PlayerJoinedMessage{
		self.NewMsgData(PlayerJoined, client),
		NewEntityData(client.id, entity.GetGraphic(), [2]int32{entity.GetX(), entity.GetY()}),
		client.username}
	self.SendToAll(joinedMsg)

	fmt.Printf("%+v\n", entity)
	// update server game state
	self.AddClient(client)
	gameManager.AddPlayer(entity)

	accepted := ConnectionAcceptedMessage{self.NewMsgData(ConnectionAccepted, client),
		NewEntityData(client.id, entity.GetGraphic(), [2]int32{entity.GetX(), entity.GetY()})}
	self.SendToClient(accepted, client)

	for _, c := range self.clients {
		if c.id == client.id {
			continue
		}
		// Send all connected player to new client
		entity := gameManager.GetEntityByID(c.id)
		clientMsg := PlayerJoinedMessage{self.NewMsgData(PlayerJoined, client), NewEntityData(c.id, entity.GetGraphic(), [2]int32{entity.GetX(), entity.GetY()}), c.username}
		self.SendToClient(clientMsg, client)
	}
}

func (self *Server) HandlePlayerTimeout(client *ServerClient) {
	left := PlayerLeftMessage{self.NewMsgData(PlayerLeft, client), client.id}
	if client.is_connected {
		self.HandlePlayerLeft(&left, client)
	}
}

func (self *Server) HandlePlayerLeft(msg *PlayerLeftMessage, client *ServerClient) {
	// TODO: make sure the client address and client player id match before doing anything
	entity := gameManager.GetEntityRefByID(msg.PlayerID)
	UpdateUser(self.database, int(client.id), entity)

	gameManager.RemoveEntityByID(msg.PlayerID)
	left := PlayerLeftMessage{self.NewMsgData(PlayerLeft, client), msg.PlayerID}
	self.RemoveClient(msg.PlayerID)
	self.SendToAll(left)
}

func (self *Server) HandlePlayerMove(msg *PlayerMovedMessage) {
	command := data.CommandMove{data.CommandKindMove, msg.Data.ID, msg.Data.Offset[0], msg.Data.Offset[1]}
	result := gameManager.SendCommandToEntity(command)
	if result {
		self.SendToAll(msg)
	} else {
		// the Command did not succesfuly happen so send
		// a correction to the user who sent the message so
		// that client and the server and the other clients aren't desynced
		client := self.clients[msg.Data.ID]
		entity := gameManager.GetEntityRefByID(msg.Data.ID)
		correctionCmd := data.CommandSetPos{data.CommandKindSetPos, msg.Data.ID, entity.GetX(), entity.GetY()}
		correction := CorrectionMessage{self.NewMsgData(Correction, client), correctionCmd}
		self.SendToClient(correction, client)
	}
}

func (self *Server) HandleAck(msg *AckMessage, client *ServerClient) {
	recTime := time.Now().UnixNano()
	if msg.ReplyTo == client.startMsgID {
		if client.pingTime == 0 {
			client.pingTime = recTime
		} else {
			client.pingTime = (client.pingTime + recTime) / 2
		}
		client.startPing = 0
		client.startMsgID = ""
	}
	client.RemoveSent(msg.ReplyTo)
}

func (self *Server) HandleChatMessage(msg *ChatNetworkMessage) {
	self.SendToAll(msg)
}

func (self *Server) HandlePing(msg *PingNetworkMessage, addr *net.UDPAddr) {
	ack := AckMessage{self.NewMsgData(Acknowledge, nil), msg.GetMsgData().ID}
	Send(ack, self.connection, addr)
}
