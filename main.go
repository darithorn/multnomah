package main

import "fmt"

var gameManager GameManager
var server Server

func main() {

	settings := LoadGameSettings("res/game.json")
	gameManager = NewGameManagerFromSettings(&settings)
	defer gameManager.Free()
	gameManager.LoadLevel("level1")

	server = NewServer(fmt.Sprintf("%s:%d", settings.ServerAddr, settings.ServerPort))
	server.Start()
	defer server.Quit()

	var running = true
	for running {
		// TODO: limit to 60fps
		gameManager.Update()
	}
}
