package main

import (
	"encoding/json"
	"game/data"
	"io/ioutil"
)

type GameSettings struct {
	ServerAddr    string
	ServerPort    int32
	DefaultScaleX int32
	DefaultScaleY int32
	SpriteWidth   int32
	SpriteHeight  int32
	SpriteSheet   string
	Font          string
	Tilemaps      map[string]string
	Sprites       map[string][2]int32
}

func LoadGameSettings(file string) GameSettings {
	fileData, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	var data GameSettings
	json.Unmarshal(fileData, &data)
	return data
}

type GameManager struct {
	playerId       int64
	settings       *GameSettings
	entityManager  EntityManager
	currentTilemap *data.Tilemap
}

func NewGameManagerFromSettings(settings *GameSettings) GameManager {
	entityManager := NewEntityManager()

	return GameManager{-1, settings, entityManager, nil}
}

func (self *GameManager) Free() {
}

func (self *GameManager) LoadLevel(level string) {
	file := self.settings.Tilemaps[level]
	tilemap := data.NewTilemapFromJson(file)
	self.currentTilemap = tilemap
}

func (self *GameManager) GetPlayerSpawn() [2]int32 {
	return self.currentTilemap.PlayerSpawn
}

func (self *GameManager) GetPlayerID() int64 {
	return self.playerId
}

func (self *GameManager) CreatePlayer(id int64, graphic string) {
	self.NewEntityWithID(id, graphic)
	self.playerId = id
}

func (self *GameManager) AddEntity(entity *data.Entity) {
	entity.NPC = true
	self.entityManager.entities[entity.GetID()] = entity
}

func (self *GameManager) AddPlayer(entity *data.Entity) {
	entity.NPC = false
	self.entityManager.players[entity.GetID()] = entity
}

func (self *GameManager) NewEntity(graphic string) int64 {
	return self.entityManager.NewEntity(graphic)
}

func (self *GameManager) NewEntityWithID(id int64, graphic string) int64 {
	return self.entityManager.NewEntityWithID(id, graphic)
}

func (self *GameManager) RemoveEntityByID(id int64) {
	self.entityManager.RemoveEntityByID(id)
}

func (self *GameManager) AtPos(id int64, x, y int32) int64 {
	e := self.entityManager.GetEntityByID(id)
	e.SetPos(x, y)
	return id
}

func (self *GameManager) GetEntityRefByID(id int64) *data.Entity {
	return self.entityManager.GetEntityByID(id)
}

func (self *GameManager) SendCommandToEntity(command data.Command) bool {
	var entity *data.Entity
	switch c := command.(type) {
	case data.CommandMove:
		// TODO: make Commands have a GetEntityID() interface function
		entity = self.entityManager.GetEntityByID(c.ID)
		if entity == nil {
			return false
		}
		x, y := entity.GetPos()
		tiles := self.currentTilemap.GetTileByPos(c.OffsetX+x, c.OffsetY+y)
		for _, tile := range tiles {
			if tile.Solid {
				return false
			}
		}
	case data.CommandSetPos:
		entity = self.entityManager.GetEntityByID(c.ID)
		if entity == nil {
			return false
		}
		tiles := self.currentTilemap.GetTileByPos(c.X, c.Y)
		for _, tile := range tiles {
			if tile.Solid {
				return false
			}
		}
	default:
		return false
	}
	return entity.SendCommand(command)
}

func (self *GameManager) GetEntityByID(id int64) data.Entity {
	return *self.entityManager.GetEntityByID(id)
}

func (self *GameManager) Update() {
	self.entityManager.Update()
}
