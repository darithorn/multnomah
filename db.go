package main

import (
	"database/sql"
	"game/data"
	"log"
)

/*
This file contains helper functions that have preconstructed queries

/----------\
|	users  |
\----------/
|	id	   | unique DB id
| username | player's username
| pos_x    | the x coordinate of the player's position
| pos_y    | the y coordinate of the player's position
| graphic  | the graphic string relating to a sprite in the spritesheet
| level    | the name of the level (map) the player is on
\----------/
*/

func InitializeDatabase(db *sql.DB) {
	stmt := `
	create table users (
		id integer primary key,
		username text not null,
		password text not null,
		pos_x integer not null,
		pos_y integer not null,
		graphic text not null,
		level text not null
		);
	`

	_, err := db.Exec(stmt)
	if err != nil {
		panic(err)
	}
}

func CreateUser(db *sql.DB, username, password, graphic, level string, x, y int32) int64 {
	stmt := `
	insert into users (username, password, pos_x, pos_y, graphic, level)
	values (?, ?, ?, ?, ?, ?);
	`

	result, err := db.Exec(stmt, username, password, x, y, graphic, level)
	if err != nil {
		log.Print(err)
		return -1
	}
	id, _ := result.LastInsertId()
	return id
}

func TestUserPassword(db *sql.DB, username, password string) bool {
	stmt := `
	select username from users where username=? and password=?;
	`

	result, err := db.Query(stmt, username, password)
	if err != nil {
		log.Print(err)
		return false
	}
	defer result.Close()
	// this will tell us if we have a result
	for result.Next() {
		var user string
		result.Scan(&user)
		// but check if usernames match just to make sure
		return user == username
	}
	return false
}

func GetUser(db *sql.DB, id int64) *data.Entity {
	stmt := `
	select username, pos_x, pos_y, graphic, level from users where id=?;
	`

	result, err := db.Query(stmt, id)
	if err != nil {
		log.Print(err)
		return &data.Entity{}
	}
	defer result.Close()
	for result.Next() {
		var username string
		var x int32
		var y int32
		var graphic string
		var level string
		result.Scan(&username, &x, &y, &graphic, &level)
		return data.NewEntityAll(username, graphic, level, id, x, y)
	}
	return nil
}

func GetUserFromName(db *sql.DB, username string) *data.Entity {
	stmt := `
	select id, pos_x, pos_y, graphic, level from users where username=?;
	`
	result, err := db.Query(stmt, username)
	if err != nil {
		log.Print(err)
		return &data.Entity{}
	}
	defer result.Close()
	for result.Next() {
		var id int64
		var x int32
		var y int32
		var graphic string
		var level string
		result.Scan(&id, &x, &y, &graphic, &level)
		return data.NewEntityAll(username, graphic, level, id, x, y)
	}
	return nil
}

func UpdateUser(db *sql.DB, id int, entity *data.Entity) {
	stmt := `
	update users set pos_x=?, pos_y=? where id=?;
	`

	result, err := db.Exec(stmt, entity.GetX(), entity.GetY(), id)
	if err != nil {
		log.Print(err)
	}
	n, err := result.RowsAffected()
	if n < 1 {
		log.Printf("No rows affected in UpdateUser %d\n", id)
	}
}
