package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"game/data"
	"net"
	"strconv"
	"time"
)

const (
	Invalid                  int32 = iota
	ConnectMessage                 = iota
	ConnectionChallenge            = iota
	ConnectionChallengeReply       = iota
	ConnectionDeclined             = iota
	ConnectionAccepted             = iota
	Acknowledge                    = iota
	Correction                     = iota
	PlayerJoined                   = iota
	PlayerLeft                     = iota
	PlayerMoved                    = iota
	Register                       = iota
	RegisterReply                  = iota
	AddEntity                      = iota
	CommandEntity                  = iota
	ChatMessage                    = iota
	PingMessage                    = iota
)

var MsgStrings = [...]string{
	"Invalid",
	"ConnectMessage",
	"ConnectionChallenge",
	"ConnectionChallengeReply",
	"ConnectionDeclined",
	"ConnectionAccepted",
	"Acknowledge",
	"Correction",
	"PlayerJoined",
	"PlayerLeft",
	"PlayerMoved",
	"Register",
	"RegisterReply",
	"AddEntity",
	"CommandEntity",
	"ChatMessage",
	"Ping"}

func FormatMsgType(i int32) string {
	length := int32(len(MsgStrings))
	if i >= length || i < 0 {
		return "Unknown"
	}
	return MsgStrings[i]
}

const (
	DeclinedWrongSalt       = iota
	DeclinedAlreadyLoggedIn = iota
	DeclinedNotRegistered   = iota
	DeclinedWrongPassword   = iota
)

const (
	RegisterSuccessful       = iota
	RegisterUsernameTaken    = iota
	RegisterUsernameTooShort = iota
	RegisterUsernameTooLong  = iota
)

type NetworkInfo interface {
	GetMsgID() int64
	GetSalt() string
}

type EntityData struct {
	ID       int64
	Graphic  string
	Position [2]int32
}

type EntityMove struct {
	ID     int64
	Offset [2]int32
}

func NewEntityData(id int64, graphic string, pos [2]int32) EntityData {
	return EntityData{id, graphic, pos}
}

type MessageData struct {
	Type     int32
	PlayerID int64
	ID       string
	Salt     string
}

type NetworkMessage interface {
	GetMsgData() MessageData
}

type GenericNetworkMessage struct {
	MsgData MessageData
	Data    interface{}
}

func (self *GenericNetworkMessage) GetMsgData() MessageData {
	return self.MsgData
}

type InvalidMessage struct {
	MsgData MessageData
}

func (c InvalidMessage) GetMsgData() MessageData {
	return c.MsgData
}

type RegisterMessage struct {
	MsgData  MessageData
	Username string
	Password string // is already hashed when the server receives it
	Graphic  string
}

func (c RegisterMessage) GetMsgData() MessageData {
	return c.MsgData
}

type RegisterReplyMessage struct {
	MsgData MessageData
	Status  int32
}

func (c RegisterReplyMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ConnectNetworkMessage struct {
	MsgData  MessageData
	Username string
	Password string
}

func (c ConnectNetworkMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ConnectionChallengeMessage struct {
	MsgData    MessageData
	ServerSalt string
}

func (c ConnectionChallengeMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ConnectionChallengeReplyMessage struct {
	MsgData  MessageData
	Graphic  string
	Username string
}

func (c ConnectionChallengeReplyMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ConnectionDeclinedMessage struct {
	MsgData MessageData
	Error   int32
}

func (c ConnectionDeclinedMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ConnectionAcceptedMessage struct {
	MsgData MessageData
	Data    EntityData
}

func (c ConnectionAcceptedMessage) GetMsgData() MessageData {
	return c.MsgData
}

type AckMessage struct {
	MsgData MessageData
	ReplyTo string // the msg id this is a reply to
}

func (c AckMessage) GetMsgData() MessageData {
	return c.MsgData
}

type CorrectionMessage struct {
	MsgData MessageData
	Command data.Command
}

func (c CorrectionMessage) GetMsgData() MessageData {
	return c.MsgData
}

type PlayerJoinedMessage struct {
	MsgData  MessageData
	Data     EntityData
	Username string
}

func (c PlayerJoinedMessage) GetMsgData() MessageData {
	return c.MsgData
}

type PlayerLeftMessage struct {
	MsgData  MessageData
	PlayerID int64
}

func (c PlayerLeftMessage) GetMsgData() MessageData {
	return c.MsgData
}

type PlayerMovedMessage struct {
	MsgData MessageData
	Data    EntityMove
}

func (c PlayerMovedMessage) GetMsgData() MessageData {
	return c.MsgData
}

type AddEntityMessage struct {
	MsgData MessageData
	Data    EntityData
}

func (c AddEntityMessage) GetMsgData() MessageData {
	return c.MsgData
}

type CommandEntityMessage struct {
	MsgData MessageData
	Command data.Command
}

func (c CommandEntityMessage) GetMsgData() MessageData {
	return c.MsgData
}

type ChatNetworkMessage struct {
	MsgData  MessageData
	Username string
	Message  string
}

func (c ChatNetworkMessage) GetMsgData() MessageData {
	return c.MsgData
}

type PingNetworkMessage struct {
	MsgData MessageData
}

func (c PingNetworkMessage) GetMsgData() MessageData {
	return c.MsgData
}

func XORSalts(a, b string) string {
	var salt bytes.Buffer
	for i := range a {
		salt.WriteByte(a[i] ^ b[i])
	}
	return salt.String()
}

func GenerateSalt() string {
	time := time.Now().Unix() // TODO: should use something more secure than the time
	hash := sha256.New()
	hash.Write([]byte(strconv.Itoa(int(time))))
	sum := hash.Sum(nil)
	return base64.URLEncoding.EncodeToString(sum)
}

func Send(msg NetworkMessage, conn *net.UDPConn, network *net.UDPAddr) {
	data, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	buffer := &bytes.Buffer{}
	err = binary.Write(buffer, binary.BigEndian, data)
	if err != nil {
		panic(err)
	}

	if network == nil {
		_, err = conn.Write(buffer.Bytes())
	} else {
		_, err = conn.WriteToUDP(buffer.Bytes(), network)
	}
	if err != nil {
		panic(err)
	}
}

func Read(conn *net.UDPConn) (NetworkMessage, int, *net.UDPAddr, error) {
	buf := make([]byte, 2048)
	n, addr, err := conn.ReadFromUDP(buf)
	if err != nil {
		return nil, 0, nil, err
	}
	length, err := strconv.Atoi(string(buf[0]))
	msgType, err := strconv.Atoi(string(buf[1 : length+1]))
	var msg NetworkMessage
	switch int32(msgType) {
	case Invalid:
		msg = new(InvalidMessage)
	case ChatMessage:
		msg = new(ChatNetworkMessage)
	case ConnectMessage:
		msg = new(ConnectNetworkMessage)
	case ConnectionChallenge:
		msg = new(ConnectionChallengeMessage)
	case ConnectionChallengeReply:
		msg = new(ConnectionChallengeReplyMessage)
	case ConnectionDeclined:
		msg = new(ConnectionDeclinedMessage)
	case ConnectionAccepted:
		msg = new(ConnectionAcceptedMessage)
	case Acknowledge:
		msg = new(AckMessage)
	case Correction:
		msg = new(CorrectionMessage)
	case PlayerJoined:
		msg = new(PlayerJoinedMessage)
	case PlayerLeft:
		msg = new(PlayerLeftMessage)
	case PlayerMoved:
		msg = new(PlayerMovedMessage)
	case Register:
		msg = new(RegisterMessage)
	case CommandEntity:
		msg = new(CommandEntityMessage)
	case PingMessage:
		msg = new(PingNetworkMessage)
	}
	json.Unmarshal(buf[length+1:n], msg)
	fmt.Printf("<- [%v]: %s\n", addr, FormatMsgType(msg.GetMsgData().Type))
	return msg, n, addr, err
}
