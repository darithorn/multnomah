package main

import "game/data"

type EntityManager struct {
	players  map[int64]*data.Entity
	entities map[int64]*data.Entity
}

func NewEntityManager() EntityManager {
	return EntityManager{make(map[int64]*data.Entity), make(map[int64]*data.Entity)}
}

func (self *EntityManager) newID() int64 {
	var i int64
	var oldi int64
	for i = range self.entities {
		if (i - oldi) != 1 {
			return oldi + 1
		}
		oldi = i
	}
	return oldi + 1
}

func (self *EntityManager) NewEntity(graphic string) int64 {
	id := self.newID()
	self.entities[id] = data.NewEntity(graphic, id)
	return id
}

func (self *EntityManager) NewEntityWithID(id int64, graphic string) int64 {
	// Potential to overrwrite existing entity
	self.entities[id] = data.NewEntity(graphic, id)
	return id
}

func (self *EntityManager) GetEntityByID(id int64) *data.Entity {
	if entity, ok := self.entities[id]; ok {
		return entity
	}
	if entity, ok := self.players[id]; ok {
		return entity
	}
	return nil
}

func (self *EntityManager) RemoveEntityByID(id int64) {
	if _, ok := self.entities[id]; ok {
		delete(self.entities, id)
	}
}

func (self *EntityManager) Update() {
	for _, e := range self.entities {
		e.Update()
	}
}
